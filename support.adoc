////
Les supports de Formatux sont publiés sous licence Creative Commons-BY-SA et sous licence Art Libre.
Vous êtes ainsi libre de copier, de diffuser et de transformer librement les œuvres dans le respect des droits de l’auteur.

    BY : Paternité. Vous devez citer le nom de l’auteur original.
    SA : Partage des Conditions Initiales à l’Identique.

Licence Creative Commons-BY-SA : https://creativecommons.org/licenses/by-sa/3.0/fr/
Licence Art Libre : http://artlibre.org/

Auteurs : Patrick Finet, Xavier Sauvignon, Antoine Le Morvan, Carl Chenet
////
= FORMATUX : Ma formation Linux
Antoine Le Morvan ; Xavier Sauvignon
Version 2.0 du 16 septembre 2019
:chapter-label: Chapitre
:checkedbox: pass:normal[+&#10004;]+]
:description: Support de cours Linux de Formatux
:docinfo:
:doctype: book
:encoding: utf-8
:experimental:
:icons: font
:icon-set: fa
:lang: fr
:numbered:
:pdf-fontsdir: ./theme/fonts/
:pdf-page-size: A4
:pdf-style: formatux-v2
:pdf-stylesdir: ./theme/
:sectnums:
:sectnumlevels: 2
:showtitle:
:source-highlighter: rouge
:source-language: bash
// define title_page to display the titlepage
:title-page:
//:toc!:
:toc: preamble
:toclevels: 2
:toc-title: Table des matières

:numbered!:
include::0000-preface.adoc[]

=== Gestion des versions

.Historique des versions du document
[width="100%",options="header",cols="1,2,4"]
|====================
| Version | Date | Observations
| 1.0 | Avril 2017 | Version initiale.
| 1.1 | Juin 2017 | Ajout des cours Nginx et Php-fpm.
| 1.2 | Juin 2017 | Ajout des cours MySQL et MySQL Master/Master.
| 1.3 | Juillet 2017 | Ajout de généralités devops.
| 1.4 | Aout 2017 | Ajout du cours Apache LAMP sous CentOS 7 (Nicolas Kovacs).
| 1.5 | Aout 2017 | Ajout du cours Jenkins et Rundeck.
| 1.6 | Février 2019 | Ajout des cours Ansible Niveau 2, Ansistrano, Asciidoc, Terraform, Varnish
| 2.0 | Septembre 2019 | Passage au format antora - Ajout des articles Git
|====================

:numbered:

:part-title: Partie 1 : Administration.
:imagesdir: ./
= Partie 1 : Administration.
[partintro]
--
image::./theme/images/logo-fondamentaux.png[scaledwidth="50%"]
include::formatux-fondamentaux/docs/modules/ROOT/pages/part-intro.adoc[]
--
:!chapter-number:
:global_path: formatux-fondamentaux/

include::formatux-fondamentaux/support-index.adoc[]

:part-title: Partie 2 : Securité.
:imagesdir: ./
= Partie 2 : Securité.
[partintro]
--
image::./theme/images/logo-securite.png[scaledwidth="50%"]
include::formatux-securite/docs/modules/ROOT/pages/part-intro.adoc[]
--
:!chapter-number:
:global_path: formatux-securite/

include::formatux-securite/support-index.adoc[]

:part-title: Partie 3 : Services.
:imagesdir: ./
= Partie 3 : Services.
[partintro]
--
image::./theme/images/logo-formatux.png[scaledwidth="50%"]
include::formatux-services/docs/modules/ROOT/pages/part-intro.adoc[]
--
:!chapter-number:
:global_path: formatux-services/

include::formatux-services/support-index.adoc[]

:part-title: Partie 4 : DevOPS.
= Partie 4 : Automatisation - DevOPS.
:global_path: formatux-devops/
[partintro]
--
:imagesdir: ./
image::./theme/images/logo-formatux.png[scaledwidth="50%"]
:imagesdir: {global_path}docs/modules/ROOT/assets/images/
include::formatux-devops/docs/modules/ROOT/pages/part-intro.adoc[]
--
:!chapter-number:
:imagesdir: ./

include::formatux-devops/support-index.adoc[]

:part-title: Partie 5 : Shell.
:imagesdir: ./
= Partie 5 : Shell.
[partintro]
--
image::./theme/images/logo-bash.png[scaledwidth="50%"]
include::formatux-bash/docs/modules/ROOT/pages/part-intro.adoc[]
--
:!chapter-number:
:global_path: formatux-bash/

include::formatux-bash/support-index.adoc[]

:part-title: Partie 6 : Annexes.
:imagesdir: ./
= Partie 6 : Annexes
[partintro]
--
image::./theme/images/logo-formatux.png[scaledwidth="50%"]
// include::formatux-annexes/docs/modules/ROOT/pages/part-intro.adoc[]
--
:!chapter-number:
:chapter-label: Annexe
:global_path: formatux-fondamentaux/

include::formatux-annexes/support-index.adoc[]

:numbered!:

:part-title: Glossaire et index.
= Glossaire et index

include::glossary.adoc[]

[index]
== Index
////////////////////////////////////////////////////////////////
The index is normally left completely empty, it's contents being
generated automatically by the DocBook toolchain.
////////////////////////////////////////////////////////////////
