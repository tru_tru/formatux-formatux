#!/bin/bash
IMG="assets/images/"

if [ -z "$1" ]
then
  echo "Please give target as first argument"
  exit 1
fi

if [ -f "versions-$1.txt" ]
then
  source versions-$1.txt
else
  echo "File versions-$1.txt does not exist"
  exit 1
fi

build_part () {
  echo "Working with ${1} on branch ${3}"
  if [ ! -d "$1" ]
  then
    git clone $2
  fi
  cd $1
  git checkout $3
  echo ""
  echo "Converting to PDF..."
  asciidoctor-pdf -t -D ../public -o ${1}.pdf support.adoc
  echo ""
  echo "Converting to EPUB..."
  mkdir -p ${IMG}
  find ./docs/ -name "*.png" -exec cp {} ${IMG} \;
  find ./docs/ -name "*.jpg" -exec cp {} ${IMG} \;
  asciidoctor-epub3 -t -a "imagesdir=assets/images" -D ../public -o ${1}.epub support.adoc
  rm -Rf ${IMG}
  cd ..
  echo ""
  echo ""
}

build_part $REPO_PART1_NAME $REPO_PART1_URL $REPO_PART1_TAG
build_part $REPO_PART2_NAME $REPO_PART2_URL $REPO_PART2_TAG
build_part $REPO_PART3_NAME $REPO_PART3_URL $REPO_PART3_TAG
build_part $REPO_PART4_NAME $REPO_PART4_URL $REPO_PART4_TAG
build_part $REPO_PART5_NAME $REPO_PART5_URL $REPO_PART5_TAG
build_part $REPO_PART6_NAME $REPO_PART6_URL $REPO_PART6_TAG

ls -l .
asciidoctor-pdf -t -D public -o formatux_maformationlinux.pdf support.adoc

# Copy all image into local images directory
mkdir -p ${IMG}
find $REPO_PART1_NAME/docs/ -name "*.png" -exec cp {} ${IMG} \;
find $REPO_PART1_NAME/docs/ -name "*.jpg" -exec cp {} ${IMG} \;
find $REPO_PART2_NAME/docs/ -name "*.png" -exec cp {} ${IMG} \;
find $REPO_PART2_NAME/docs/ -name "*.jpg" -exec cp {} ${IMG} \;
find $REPO_PART3_NAME/docs/ -name "*.png" -exec cp {} ${IMG} \;
find $REPO_PART3_NAME/docs/ -name "*.jpg" -exec cp {} ${IMG} \;
find $REPO_PART4_NAME/docs/ -name "*.png" -exec cp {} ${IMG} \;
find $REPO_PART4_NAME/docs/ -name "*.jpg" -exec cp {} ${IMG} \;
find $REPO_PART5_NAME/docs/ -name "*.png" -exec cp {} ${IMG} \;
find $REPO_PART5_NAME/docs/ -name "*.jpg" -exec cp {} ${IMG} \;
find $REPO_PART6_NAME/docs/ -name "*.png" -exec cp {} ${IMG} \;
find $REPO_PART6_NAME/docs/ -name "*.jpg" -exec cp {} ${IMG} \;

asciidoctor-epub3 -t -a "imagesdir=assets/images" -D ./public -o formatux_maformationlinux.epub support.adoc
rm -Rf ${IMG}

# Make index page to allow direct click on webpage
asciidoctor -D public index.adoc
